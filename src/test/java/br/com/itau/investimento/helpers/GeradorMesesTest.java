package br.com.itau.investimento.helpers;

import static org.junit.Assert.assertEquals;

import java.time.Month;

import org.junit.Before;
import org.junit.Test;

public class GeradorMesesTest {
	
	GeradorMeses gerador;
	
	@Before
	public void inicializar() {
		gerador = new GeradorMeses();
	}
	
	@Test
	public void testarInicializacao() {
		assertEquals(Month.JANUARY, gerador.obterProximoMes());
	}
	
	@Test
	public void testarObterProximoMes() {
		gerador.obterProximoMes();
		
		assertEquals(Month.FEBRUARY, gerador.obterProximoMes());
	}
	
	@Test
	public void testarReniciarMeses() {
		for(int i = 0; i < 12; i++) {
			gerador.obterProximoMes();
		}
		
		assertEquals(Month.JANUARY, gerador.obterProximoMes());
	}
}
